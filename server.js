
var mysql=require('mysql'); //importation du module mysql
var express=require('express');//
var LocalStorage= require('node-localstorage').LocalStorage, localStorage = new LocalStorage('./scratch');
var session=require('express-session');

var redirect = require('express-simple-redirect');

var bodyParser=require('body-parser');
var app=express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies



app.use(express.static(__dirname + ''));

app.use(function (req, res, next) {
    // autorise quelle site a envoyer des donné (ici tout le monde)
    res.setHeader('Access-Control-Allow-Origin', '*');
    // quelle type de requete sont autoriser
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // OBLIGER SINON PAS DE RECEPTION DE DATA !!
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Pour l'API
    res.setHeader('Access-Control-Allow-Credentials', true);
    //Pour continuer dnas les autres function
    next();
});



	//intialisation des paramètres de la BD
	var con=mysql.createConnection({
		host: "localhost",
		user: "root",
		password: "",
		database: "doodlecalendar"
	});

	//connexion à la BD
	con.connect(); //méthode existe déjà dans la BD
	var sess;
//recupere les informations

	app.get("/",function(req,res){
		
		res.sendFile("index.html", {root: __dirname});

	});
//traite les informations
	app.post("/signup",function(req,res){

		var data={ 
			nom: req.body.nom, 
			prenom: req.body.prenom,
			color: req.body.color,
			username: req.body.username2,
			password: req.body.pwd1
		};
		var confirm=req.body.pwd2;
		//console.log(data);
		//console.log(data.password,confirm);
		var user=req.body.username2;
			//console.log(user);
		if(data.nom!="" && data.prenom!="" && data.color!="" && data.username!="" && data.password!="" && confirm!=""){

	        	if (data.password===confirm){

	        		con.query('Select * from user where username = ? ', [user] ,function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
						
							    if (results==""){
									con.query('INSERT INTO user SET ?',data,
										function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
											if(results != ""){
												res.send({reponse: "success", info: "Insertion valide"});
											}else{
												var msg = "Paramters undefined echec Insert";
												res.send({reponse: "error", info: msg});
											}

										});
									var msg="username inseré";
									res.send({reponse: "error", info: msg});
								
							    } else {
								var msg="username existe déjà";
								res.send({reponse: "error", info: msg});
							    }

					});

				} else {
	        		 var res1="VERIFIER VOTRE MDP";
	                 console.log(res1);
	            }
        } else {
         var res="Veuillez completer tout les champs";
         console.log(res);

        }
});

	app.post("/login",function(req,res){
		/*	res.sendFile("index.html", {root: __dirname});*/
			var username = req.body.username;
			var password = req.body.pwd; 
			
			

			con.query('SELECT * FROM user WHERE username = ? AND password = ?', [username,password] ,function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
				
				if(results != ""){
					var ids = results[0].id;

					//console.log(results);
				 	localStorage.setItem("token",  ids);
					//redirection sur la page d'accueil
				
					res.send({reponse: "success", info: "username et password sont bon"});
					 
				}else{
					var msg = "Username or password undefined";
					console.log(msg);
					res.send({reponse: "error", info: "username et password sont mauvais"});
				}
				
			}); 

	});

	app.get("/logout",function(req,res){
		var sess=localStorage.removeItem("token");
		res.redirect('/');
	});


	app.get("/listecalendrier",function(req,res){
		var iduser= localStorage.getItem("token");
		

		con.query('SELECT * FROM agenda WHERE iduser = ?', iduser ,function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
				
				//console.log(results)
				if(results != ""){
					res.send({results});
				}else{
					var msg = "undefined";
					res.send({reponse: "error", info: msg});
				}

				
			}); 
	}); 
     

     app.get("/listecalendrierinviter",function(req,res){
		var iduser= localStorage.getItem("token");
		

		con.query('SELECT * FROM agenda LEFT JOIN invite ON agenda.id = invite.idagenda WHERE invite.iduser = ?', iduser ,function(error,results){
				

				//console.log("FDDFG"+results)
				if(results != ""){
					res.send({results});
				}else{
					var msg = "undefined";
					res.send({reponse: "error", info: msg});
				}
				
			}); 
	});

	app.post("/ajouter",function(req,res){
        var iduser= localStorage.getItem("token");
     
		var data={ /*
			dateusers: req.body.date,*/
			iduser: iduser,
            titre: req.body.titre, 
			description: req.body.description
		};

		

		if(data.titre!="" && data.description!=""){

	 
				con.query('INSERT INTO agenda SET ?',data,
					function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
						if(results != ""){

							res.redirect('/home');
							//res.send({reponse: "success", info: "Insertion valide"});
						}else{
							res.send({reponse: "error", info: "Insertion non valide"});
						}

					});
								
								
		} else{ 
			res.send({reponse: "error", info: "Champ vide"});

		} 
		
	}); 

	app.get("/preference/:id",function(req,res){
		var sess = localStorage.getItem("token");

		var id=req.params.id;
		if (sess) {
			res.sendFile("preference.html", {root: __dirname});
		}else{
			res.redirect('/');
		}
	});


	app.get("/invite/:id",function(req,res){
		var sess = localStorage.getItem("token");

		var id=req.params.id;

		if (sess) {
			res.sendFile("invite.html", {root: __dirname});
		}else{
			res.redirect('/');
		}
	});

	app.post("/listepreferenceinvite",function(req,res){
		
		var idAgenda = req.body.idCalendar;
		con.query('SELECT * FROM agenda LEFT JOIN preference ON agenda.id = preference.idagenda WHERE preference.idagenda = ?', idAgenda ,function(error,results){ 
				
				if(results != ""){
					res.send({results});
				}else{
					var msg = "undefined";
					res.send({reponse: "error", info: msg});
				}	
		}); 
	});



	app.get("/home",function(req,res){
		var sess = localStorage.getItem("token");

		if (sess) {
			res.sendFile("home.html", {root: __dirname});
		}else{
			res.redirect('/');
		}
	});



	app.post("/listepreference",function(req,res){
		
		var idAgenda = req.body.idCalendar;
		con.query('SELECT * FROM agenda LEFT JOIN preference ON agenda.id = preference.idagenda WHERE preference.idagenda = ?', idAgenda ,function(error,results){ 
				
				if(results != ""){
					res.send({results});
				}else{
					var msg = "undefined";
					res.send({reponse: "error", info: msg});
				}	
		}); 
	});



	app.post("/insertpreference",function(req,res){
		
		var datepreference = req.body.datepreference;
		var idCalendar     = req.body.idCalendar;

		var data={ 
			idagenda: idCalendar,
            preferencedate: datepreference
		};

		if(data.idagenda!="" && data.preferencedate!=""){
			con.query('INSERT INTO preference SET ?',data,
			function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
				if(results != ""){
					res.redirect('/home');
					//res.send({reponse: "success", info: "Insertion valide"});
				}else{
					res.send({reponse: "error", info: "Insertion non valide"});
				}
			});
		} else{ 
			res.send({reponse: "error", info: "Champ vide"});
		} 
	});


	// ====================================================================
	//=====================================================================
	app.get("/vote/:id",function(req,res){
		var sess = localStorage.getItem("token");

		var id=req.params.id;

		if (sess) {
			res.sendFile("vote.html", {root: __dirname});
		}else{
			res.redirect('/');
		}
	});


	app.post("/verifvotes",function(req,res){
		var sess = localStorage.getItem("token");

		var idPreference = req.body.idPreference;
		var iduser       = sess;

		var data={ 
			idpreference: idPreference,
            idUser: iduser
		};

        con.query('SELECT * FROM vote WHERE idUser=? AND idPreference=?', [iduser, idPreference], function(error,results){
    		if(results == ""){
				res.send({reponse: "success", info: "vote ok"});
			}else{
				res.send({reponse: "error", info: "vote undefined"});
			}
        });
		

	});


	app.post("/addvotes",function(req,res){
		var sess = localStorage.getItem("token");

		var idPreference = req.body.idPreference;
		var iduser       = sess;

		var data={ 
			idpreference: idPreference,
            idUser: iduser
		};

		console.log(data);
        con.query('INSERT INTO vote SET ?', data, function(error,results){
    		if(results!==""){
    			res.send({reponse: "success", info: "vote valide"});
    		}else{ 
    			res.send({reponse: "error", info: "vote non valide"});
    		}
        });
	});

	app.post("/removevotes",function(req,res){
		var sess = localStorage.getItem("token");

		var idPreference = req.body.idPreference;
		var iduser       = sess;

		var data=[  iduser, idPreference ];
		console.log(data)

		con.query('DELETE FROM vote WHERE idUser = ? AND idpreference = ?',data ,function (error, results) {
		  if(results != ""){
				res.send({reponse: "success", info: results});
			}else{
				res.send({reponse: "error", info: "vote non delete"});
			}
		  
		})

	});



	app.post("/viewsvotes",function(req,res){
		var sess = localStorage.getItem("token");

		var idPreference = req.body.idPreference;

		con.query('SELECT * FROM vote LEFT JOIN user ON vote.idUser = user.id WHERE vote.idpreference = ?',idPreference ,function (error, results) {
		  if(results != ""){
				res.send({reponse: "success", info: results});
			}else{
				res.send({reponse: "error", info: "vote undefined"});
			}
		  
		})

	});

	// ====================================================================
	//=====================================================================


	app.post("/inviteuser",function(req,res){
		
		var contentUser = req.body.contentUser;

		if(contentUser !=""){
			con.query('SELECT * FROM user WHERE username = ?',contentUser,
			function(error,results){ // query possede 2 paramètres "la requête" et "la fonction callback"
				if(results != ""){
					res.send({reponse: "success", info: results});
				}else{
					res.send({reponse: "error", info: "Invitation non valide"});
				}
			});
		} else{ 
			res.send({reponse: "error", info: "Champ vide"});
		} 
	}); 

	app.post("/addinviteuser",function(req,res){

		var data={ 
			iduser: req.body.iduser,
            idagenda: req.body.idCalendar
		};

		if(data.iduser !="" && data.idagenda !=""){
			con.query('INSERT INTO invite SET ?',data,
			function(error,results){
				if(results != ""){
					res.send({reponse: "success", info: "user inviter"});
				}else{
					res.send({reponse: "error", info: "Invitation non valide"});
				}
			});
		} else{ 
			res.send({reponse: "error", info: "Champ vide"});
		} 
	});

	app.post("/showinviteuser",function(req,res){

		var data={ 
            idagenda: req.body.idCalendar
		};

		if(data.idagenda !=""){
			con.query('SELECT * FROM agenda LEFT JOIN invite ON agenda.id = invite.idagenda LEFT JOIN user ON invite.iduser = user.id WHERE invite.idagenda = ?',data.idagenda,
			function(error,results){
				if(results != ""){
					res.send({reponse: "success", info: results });
				}else{
					res.send({reponse: "error", info: "Invitation non valide"});
				}
			});
		} else{ 
			res.send({reponse: "error", info: "Champ vide"});
		} 
	});
	


app.listen(3000);







	


	/*con.query('UPDATE user SET color="pink" WHERE nom="Mouchir"', function (error, results, fields) {
	  if (error) throw error;
	  console.log('changed ' + results.changedRows + ' rows');
	});*/
