-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 02 Avril 2017 à 12:50
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `doodlecalendar`
--
CREATE DATABASE IF NOT EXISTS `doodlecalendar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `doodlecalendar`;

-- --------------------------------------------------------

--
-- Structure de la table `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `titre` varchar(55) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `agenda`
--

INSERT INTO `agenda` (`id`, `iduser`, `titre`, `description`) VALUES
(6, 1, 'aaa', 'qqqqqqqqqq'),
(7, 1, 'aaa', 'bonjour'),
(8, 1, 'tezsdr', 'hello'),
(9, 1, 'nodes', 'test'),
(10, 1, 'nodes', 'test'),
(11, 1, 'package', 'bonjour'),
(12, 7, 'aaaaa', 'aaaaaaaa'),
(13, 7, 'aaaaa', 'aaaaaaaa'),
(14, 1, 'help javascript', 'nothing');

-- --------------------------------------------------------

--
-- Structure de la table `invite`
--

CREATE TABLE IF NOT EXISTS `invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idagenda` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `color` varchar(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `color`, `username`, `password`) VALUES
(1, 'MOUCHIR', 'BACHIRA', 'pink', 'bach', 'hello'),
(7, 'MOUCHIR jonathan', 'john', '#ff8000', 'light', 'hello'),
(8, 'aaaaaaaaaaaa', 'aaaaaaaaaaa', '#000000', 'aaaaaaaaaa', 'aaaaaaaaaaaa'),
(9, 'zzzzzzzzzzz', 'zzzzzzzzzzzz', '#000000', 'azzzzzzzzz', 'zaaaaaaaa'),
(11, 'bachira', 'bachira', '#000000', 'hhhh', 'bach'),
(12, 'aaa', 'aaaaaaa', '#000000', 'aaaaaaaa', 'hello'),
(14, 'hhhh', 'jjjjjjjj', '#000000', 'j', 'hi'),
(15, 'hello', 'hello', '#000000', 'hello', 'hello'),
(16, 'ss', 'sssss²', '#000000', 'qqqq', 'bjr'),
(17, 'bonjour', 'bonsoir', '#000000', 'bjr', 'hello'),
(18, 'salut', 'salut', '#000000', 'salut', 'salut'),
(19, 'gggg', 'ggggg', '#000000', 'hhhhh', 'hello'),
(20, 'dsa', 'ddfghj', '#000000', 'derfg', 'hello'),
(21, 'pape', 'pape', '#000000', 'pape', 'pape'),
(22, 'test', 'test', '#00ff40', 'tests', 'bonjour'),
(23, 'tester', 'tester', '#ff80c0', 'tester', 'bonjour');

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE IF NOT EXISTS `vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idAgenda` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
